##story2
* out_of_scope
    - utter_out_of_scope
    - action_restart

##story3
* goodbye
    - utter_goodbye

##story4
* about_loan
    - actionshowtypeofloan
    - action_restart

## story_01
* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}
    - action_restart

## New Story
* about_loan
     - actionshowtypeofloan
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"education loan"}
    - slot{"type_of_loan":"education loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"education loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"education loan"}
    - slot{"type_of_loan":"education loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"education loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"hire purchase loan"}
    - slot{"type_of_loan":"hire purchase loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"hire purchase loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"hire purchase loan"}
    - slot{"type_of_loan":"hire purchase loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"hire purchase loan"}
    - action_restart

## New Story
* about_loan
     - actionshowtypeofloan
* about_loan{"type_of_loan":"home loan"}
    - slot{"type_of_loan":"home loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"home loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"home loan"}
    - slot{"type_of_loan":"home loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"home loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"industry loan"}
    - slot{"type_of_loan":"industry loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"industry loan"}
    - action_restart

## New Story
* about_loan{"type_of_loan":"industry loan"}
    - slot{"type_of_loan":"industry loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"industry loan"}
    - action_restart

## New Story
* about
    - utter_about
    - action_restart

##New Story
* location
    - utter_location
    - action_restart

##New Story
* contact
    - utter_contact
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"education loan"}
    - slot{"type_of_loan":"education loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"education loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"hire purchase loan"}
    - slot{"type_of_loan":"hire purchase loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"hire purchase loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"home loan"}
    - slot{"type_of_loan":"home loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"home loan"}
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"industry loan"}
    - slot{"type_of_loan":"industry loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"industry loan"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"remittance"}
    - slot{"type_of_services":"remittance"}
    - fetchtypeofservices
    - slot{"type_of_services":"remittance"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"deposit"}
    - slot{"type_of_services":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":"deposit"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

## New Story
* contact
    - utter_contact
    - action_restart

## New Story
* contact
    - utter_contact
    - action_restart

## New Story
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

## New Story
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

##New Story
* about_service_center
     - utter_about_service_center
     - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
    - action_restart

## New Story
* services
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
    - action_restart

## New Story

* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story

* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
* goodbye
	- utter_goodbye
    - action_restart

## New Story
* interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
* goodbye
	- utter_goodbye
    - action_restart

## New Story
* location
    - utter_location
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"remittance"}
    - slot{"type_of_services":"remittance"}
    - fetchtypeofservices
    - slot{"type_of_services":"remittance"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"deposit"}
    - slot{"type_of_services":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":"deposit"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"deposit"}
    - slot{"type_of_services":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":"deposit"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_services":"remittance"}
    - slot{"type_of_services":"remittance"}
    - fetchtypeofservices
    - slot{"type_of_services":"remittance"}
* goodbye
    - utter_goodbye
    - action_restart

## New Story
* location
    - utter_location
* services
    - actionshowtypeofservices
* services{"type_of_services":"sms banking"}
    - slot{"type_of_services":"sms banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"sms banking"}
    - action_restart

## New Story
* out_of_scope
    - utter_out_of_scope
    - action_restart

## New Story
* contact
    - utter_contact
    - action_restart

## New Story
* services
    - actionshowtypeofservices
* services{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":null}
    - action_restart

## New Story

* services{"type_of_services":"sms banking"}
    - slot{"type_of_services":"sms banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"sms banking"}
    - action_restart

## New Story

* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
    - action_restart

## New Story

* services{"type_of_services":"atm"}
    - slot{"type_of_services":"atm"}
    - fetchtypeofservices
    - slot{"type_of_services":"atm"}
    - action_restart

## New Story

* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}
    - action_restart

## New Story

* about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}
    - action_restart

## New Story

* about_loan
    - actionshowtypeofloan
    - action_restart
