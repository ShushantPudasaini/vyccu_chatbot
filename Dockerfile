FROM python:3.6
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN apt install -y supervisor
ADD . /app
WORKDIR /app
RUN pip3 install spacy
RUN pip3 install rasa
#RUN pip3 install rasa[spacy]
RUN python3 -m spacy download en_core_web_md
RUN python3 -m spacy link en_core_web_md en
#RUN pip3 install rasa==1.2.3
#RUN pip3 install rasa-sdk==1.1.1
RUN rasa train
#CMD rasa run --port 5005
#CMD rasa run actions --port 5055
COPY ./init/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
CMD /usr/bin/supervisord


